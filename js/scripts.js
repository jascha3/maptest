(function() {
	function toJSONString( form ) {
		var obj = {};
		var elements = form.querySelectorAll( "input, select, textarea" );
		for( var i = 0; i < elements.length; ++i ) {
			var element = elements[i];
			var name = element.name;
			var value = element.value;

			if( name ) {
				obj[ name ] = value;
			}
		}

		return JSON.stringify( obj );
	}

	document.addEventListener( "DOMContentLoaded", function() {
		var form = document.getElementById( "test" );
		var output = document.getElementById( "output" );
		var viewDiv = document.getElementById( "viewDiv" );
		form.addEventListener( "submit", function( e ) {
			e.preventDefault();
			jsonfile = toJSONString( this );
			jsonfile = JSON.parse(jsonfile);
			jsonfile = jsonfile.jsonfield;
			// var geojson = geojson.jsonfield;
			output.innerHTML = jsonfile;
			//map.getLayer("layer").refresh();  
			document.getElementById('mapframe').src = document.getElementById('mapframe').src
			document.getElementById('mapframe2').src = document.getElementById('mapframe2').src
    		// viewDiv.reload();
		}, false);

	});

})();

